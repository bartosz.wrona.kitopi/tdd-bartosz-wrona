# tdd-101

### The goal
The aim of this repository is to teach you basics of TDD approach in very simple steps. If you're new to TDD, here's something to [read](https://testdriven.io/blog/modern-tdd/) 

### Installation
To start off install requirements:
```pip install -r requirements.txt```

## The exercise
Your aim is to create a SDK that is able to process input CSV file containing titles of car auctions and their prices and expose it as objects.
1. User should be able to load the data from CSV file via `CarManager` helper
2. User should be able to get list of `Car` objects from `CarManager`
3. Each object should have properties:
   - `brand` string value describing the brand of the car
   - `model` string value describing the model of the car
   - `price` instance of the `CarPrice` object  
4. `CarPrice` should have `value` (float) and `currency` (string)