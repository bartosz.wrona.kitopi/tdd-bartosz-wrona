import csv
from typing import List


class Car:
    def __init__(self, title_string, price_string):
        self.title_string = title_string
        self.price_string = price_string


class CarManager:
    def __init__(self):
        self.cars = []

    def load_from_csv(self, csv_path):
        with open(csv_path) as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=';')
            for row in csvreader:
                self.cars.append(Car(row['title'], row['price']))

    def get_cars(self) -> List[Car]:
        return self.cars
